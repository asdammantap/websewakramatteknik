-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2015 at 12:51 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbsewakramatteknik`
--

-- --------------------------------------------------------

--
-- Table structure for table `alat`
--

CREATE TABLE IF NOT EXISTS `alat` (
  `no_alat` char(11) NOT NULL DEFAULT '',
  `nama_alat` varchar(200) DEFAULT NULL,
  `jenis_alat` varchar(10) DEFAULT NULL,
  `deskripsi` text,
  `harga` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alat`
--

INSERT INTO `alat` (`no_alat`, `nama_alat`, `jenis_alat`, `deskripsi`, `harga`, `stok`, `gambar`) VALUES
('AB01', 'Kobelco', 'Dum Truck', NULL, 12000, 10, 'Dumtruck.jpg'),
('AS01', 'AJSKAJKA', 'DJAKADJK', 'DJAKDJAKJADKAJDK\r\n', 1000, 10, '45Buldozer_Caterpillar.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE IF NOT EXISTS `mobil` (
  `no_pol` char(11) NOT NULL DEFAULT '',
  `jenis` varchar(20) DEFAULT NULL,
  `merk` varchar(30) DEFAULT NULL,
  `warna` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `username` char(11) NOT NULL DEFAULT '',
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `password`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `penyewa`
--

CREATE TABLE IF NOT EXISTS `penyewa` (
  `no_penyewa` char(11) NOT NULL DEFAULT '',
  `nama_penyewa` varchar(50) DEFAULT NULL,
  `alamat` text,
  `nohp` varchar(15) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penyewa`
--

INSERT INTO `penyewa` (`no_penyewa`, `nama_penyewa`, `alamat`, `nohp`, `email`, `password`) VALUES
('80', 'ASAL', 'Cigading', '08763632', 'as2605dam@gmail.com', '202cb962ac59075b964b07152d234b70'),
('U789', 'FGFGF', 'ERER', '0876543211', 'asal@gmail.com', 'QWQWQW');

-- --------------------------------------------------------

--
-- Table structure for table `sewa_detail`
--

CREATE TABLE IF NOT EXISTS `sewa_detail` (
  `no_sewa` int(6) NOT NULL DEFAULT '0',
  `no_alat` char(6) NOT NULL DEFAULT '',
  `banyak` int(11) DEFAULT NULL,
  `totaljam` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sewa_header`
--

CREATE TABLE IF NOT EXISTS `sewa_header` (
`no_sewa` int(6) NOT NULL,
  `tgl_sewa` date DEFAULT NULL,
  `jam_sewa` time DEFAULT NULL,
  `no_penyewa` char(11) DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sewa_header`
--

INSERT INTO `sewa_header` (`no_sewa`, `tgl_sewa`, `jam_sewa`, `no_penyewa`) VALUES
(1, '2015-06-23', '06:02:24', '80'),
(2, '2015-06-23', '06:08:14', '80'),
(3, '2015-06-23', '10:00:25', '80'),
(4, '2015-06-23', '10:06:17', '80');

-- --------------------------------------------------------

--
-- Table structure for table `sewa_sementara`
--

CREATE TABLE IF NOT EXISTS `sewa_sementara` (
`no_sewa_sementara` int(11) NOT NULL,
  `tgl_sewa_sementara` date DEFAULT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `no_alat` char(5) DEFAULT NULL,
  `banyak` int(11) DEFAULT NULL,
  `totaljam` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alat`
--
ALTER TABLE `alat`
 ADD PRIMARY KEY (`no_alat`);

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
 ADD PRIMARY KEY (`no_pol`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `penyewa`
--
ALTER TABLE `penyewa`
 ADD PRIMARY KEY (`no_penyewa`);

--
-- Indexes for table `sewa_detail`
--
ALTER TABLE `sewa_detail`
 ADD KEY `no_alat` (`no_alat`);

--
-- Indexes for table `sewa_header`
--
ALTER TABLE `sewa_header`
 ADD PRIMARY KEY (`no_sewa`), ADD KEY `no_penyewa` (`no_penyewa`);

--
-- Indexes for table `sewa_sementara`
--
ALTER TABLE `sewa_sementara`
 ADD PRIMARY KEY (`no_sewa_sementara`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sewa_header`
--
ALTER TABLE `sewa_header`
MODIFY `no_sewa` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sewa_sementara`
--
ALTER TABLE `sewa_sementara`
MODIFY `no_sewa_sementara` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sewa_detail`
--
ALTER TABLE `sewa_detail`
ADD CONSTRAINT `sewa_detail_ibfk_2` FOREIGN KEY (`no_alat`) REFERENCES `alat` (`no_alat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sewa_header`
--
ALTER TABLE `sewa_header`
ADD CONSTRAINT `sewa_header_ibfk_1` FOREIGN KEY (`no_penyewa`) REFERENCES `penyewa` (`no_penyewa`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
